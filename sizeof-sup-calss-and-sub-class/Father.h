//
// Created by hank on 2020/1/29.
//

#ifndef CPP_DEMO_CLION_FATHER_H
#define CPP_DEMO_CLION_FATHER_H

#include <iostream>
#include <string>

using namespace std;

class Father {
public:
    Father(string name, int age) {
        this->name = name;
        this->age = age;
    }

    virtual void print() {
        cout << "Father sizeof(*this): " << sizeof(*this) << endl;
        cout << this->name << " " << this->age << endl;
    }

protected:
    string name;
    int age;
};

#endif //CPP_DEMO_CLION_FATHER_H
