#include <iostream>
#include "Father.h"
#include "Child.h"
using namespace std;

void test() {
    Father f("a", 10);
    Child fc("a", 10, 1);
    Child c("a", 10, 0);
    Father fcc = c;

    Father *fp = nullptr;
    Father *fcp = nullptr;
    Child *cp = nullptr;

    fp = &f;
    fcp = &fc;
    cp = &c;

    Father fa[3] = {f, fc, c};

    cout << "sizeof(fcc): " << sizeof(fcc) << endl;
    fcc.print();

    cout << "sizeof(*fp): " << sizeof(*fp) << endl;
    cout << "sizeof(*fcp): " << sizeof(*fcp) << endl;
    cout << "sizeof(*cp): " << sizeof(*cp) << endl;

    fp->print();
    fcp->print();
    cp->print();

    cout << "sizeof(fa[0]): " << sizeof(fa[0]) << endl;
    cout << "sizeof(fa[1]): " << sizeof(fa[1]) << endl;
    cout << "sizeof(fa[2]): " << sizeof(fa[2]) << endl;

    fa[0].print();
    fa[1].print();
    fa[2].print();
}