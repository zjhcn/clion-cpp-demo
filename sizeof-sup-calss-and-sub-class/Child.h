//
// Created by hank on 2020/1/29.
//

#ifndef CPP_DEMO_CLION_CHILD_H
#define CPP_DEMO_CLION_CHILD_H

#include <string>
#include "Father.h"

using std::string;

class Child : public Father {
public:
    Child(string name, int age, int gender) : Father(name, age) {
        this->gender = gender;
        this->nickname = name + " nickname";
    }
    void print() {
        cout << "Child sizeof(*this): " << sizeof(*this) << endl;
        cout << this->name << " " << this->age << endl;
        cout << this->nickname << " " << this->gender << endl;
    }

private:
    string nickname;
    int gender;
};

#endif //CPP_DEMO_CLION_CHILD_H
